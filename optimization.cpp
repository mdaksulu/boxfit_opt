//
// Created by Mehmet Deniz Aksulu on 2019-04-02.
//

#include "optimization.h"
#include "opt/multinest_wrapper.h"

std::vector<double> _min_params(no_fit_vars_);
std::vector<double> _max_params(no_fit_vars_);
std::vector<double> _min_host_flux;
std::vector<double> _max_host_flux;
int fit_host = 0;
std::vector<double> host_bands;
double band_margin;
int no_host_bands;
int log_host;
double log_host_minmax_ratio;
int silent = 0;
void optimize(double *var_min,
              double *var_max,
              int data_count,
              double *t_obs,
              double *nu_obs,
              double *flux_obs,
              double *sigma_obs) {
  string parfilename = "boxfitsettings.txt";

  fit_host = int_from_parfile((char *) parfilename.c_str(), "fit_host");
  if (fit_host == 1) {
    band_margin = double_from_parfile((char *) parfilename.c_str(), "band_margin");
    no_host_bands = int_from_parfile((char *) parfilename.c_str(), "no_host_bands");
    log_host = int_from_parfile((char *) parfilename.c_str(), "log_host");
    log_host_minmax_ratio = double_from_parfile((char *) parfilename.c_str(), "log_host_minmax_ratio");
    _max_host_flux.resize(no_host_bands);
    _min_host_flux.resize(no_host_bands, 0);
    host_bands.resize(no_host_bands);
    for (int i = 0; i < no_host_bands; i++) {
      char par_name[256];
      sprintf(par_name, "host_band_%d", i);
      host_bands[i] = double_from_parfile((char *) parfilename.c_str(), par_name);
      std::vector<int> indices = get_band_indices(nu_obs, host_bands[i], data_count, band_margin);
      std::vector<double> flux(indices.size());
      for (int j = 0; j < flux.size(); j++) {
        flux[j] = flux_obs[indices[j]] + sigma_obs[indices[j]];
      }
      double min_flux = *min_element(flux.begin(), flux.end());
      _max_host_flux[i] = min_flux;
      if (log_host == 1) {
        _max_host_flux[i] = log10(min_flux);
        _min_host_flux[i] = log10(min_flux) + log_host_minmax_ratio;
      }
    }
  }

  boxfit_args args;
  args.theta_obs_frac = int_from_parfile((char *) parfilename.c_str(), "theta_obs_frac");
  args.theta_0_log = int_from_parfile((char *) parfilename.c_str(), "theta_0_log");;
  args.data_count = data_count;
  args.t_data = t_obs;
  args.nu_data = nu_obs;
  args.flux_data = flux_obs;
  args.sigma_data = sigma_obs;

  for (int i = 0; i < no_fit_vars_; i++) {
    if (i == fit_theta_0_ && args.theta_0_log == 1) {
      _min_params[i] = log10(var_min[i]);
      _max_params[i] = log10(var_max[i]);
    } else if (i == fit_theta_0_ && args.theta_0_log == 2) {
      _min_params[i] = log(var_min[i]);
      _max_params[i] = log(var_max[i]);
    } else {
      _min_params[i] = var_min[i];
      _max_params[i] = var_max[i];
    }
  }
  if (fit_host == 1) {
    for (int i = 0; i < _min_host_flux.size(); i++) {
      _min_params.push_back(_min_host_flux[i]);
      _max_params.push_back(_max_host_flux[i]);
    }
  }
  args.min_parameters = _min_params;
  args.max_parameters = _max_params;

  multinest_wrapper multinest(_min_params, _max_params);
  multinest_properties properties;
  properties.IS = int_from_parfile((char *) parfilename.c_str(), "IS");
  properties.mmodal = int_from_parfile((char *) parfilename.c_str(), "mmodal");
  properties.nClsPar = int_from_parfile((char *) parfilename.c_str(), "nClsPar");
  properties.ceff = int_from_parfile((char *) parfilename.c_str(), "ceff");
  properties.nlive = int_from_parfile((char *) parfilename.c_str(), "nlive");
  properties.efr = double_from_parfile((char *) parfilename.c_str(), "efr");
  properties.tol = double_from_parfile((char *) parfilename.c_str(), "tol");
  properties.updInt = int_from_parfile((char *) parfilename.c_str(), "updInt");
  properties.maxModes = int_from_parfile((char *) parfilename.c_str(), "maxModes");
  properties.fb = int_from_parfile((char *) parfilename.c_str(), "fb");
  properties.maxiter = int_from_parfile((char *) parfilename.c_str(), "maxiter");
  properties.resume = int_from_parfile((char *) parfilename.c_str(), "resume");
  properties.silent = int_from_parfile((char *) parfilename.c_str(), "silent");
  silent = properties.silent;
  char *temp_str = new char[256];
  string_from_parfile((char *) parfilename.c_str(), "output_dir", temp_str);
  std::string output_dir(temp_str);
  properties.output_dir = output_dir;

  multinest.SetProperties(properties);
  multinest.fit(func_fitness_serial, &args);
}

double hard_bounds_min[no_fit_vars_] = {0.01, 46, -6, 0.0, 2.0, -10, -10, -10};
double hard_bounds_max[no_fit_vars_] = {M_PI_2, 58, 6, M_PI_2, 4.0, 0, 0, 0};

double func_fitness(std::vector<double> _parameters, void *_args) {
#if OPEN_MPI_ == ENABLED_
  boxfit_args *args = (boxfit_args *) _args;
  double fitvars[no_fit_vars_];
  for (int i = 0; i < no_fit_vars_; i++) {
    if (i == fit_theta_0_ && args->theta_0_log == 1) {
      fitvars[i] = pow(10, _parameters[i]);
    } else if (i == fit_theta_0_ && args->theta_0_log == 2) {
      fitvars[i] = exp(_parameters[i]);
    } else if (i == fit_theta_obs_ && args->theta_obs_frac) {
      fitvars[i] = _parameters[i] * fitvars[fit_theta_0_];
    } else {
      fitvars[i] = _parameters[i];
    }
  }
  for (int i = 0; i < no_fit_vars_; i++) {
    if (fitvars[i] < hard_bounds_min[i] || fitvars[i] > hard_bounds_max[i]) {
      return -1 * std::numeric_limits<double>::max();
    }
  }

  int data_count = args->data_count;
  double *t_obs = args->t_data;
  double *nu_obs = args->nu_data;
  double *flux_obs = args->flux_data;
  double *sigma_obs = args->sigma_data;

  int i;
  int idle_id;
  MPI_Status err;
  int stopsignal = -1;

  double chi_squared[data_count];
  double chi_squaredTemp[data_count]; // temp buffer to collect results
  double calculated_flux_temp[data_count]; // temp buffer to collect results

  // clean out Ftemp and Fresult
  for (i = 0; i < data_count; i++) {
    chi_squared[i] = 0.0;
    chi_squaredTemp[i] = 0.0;
    calculated_flux_temp[i] = 0.0;
  }

  prepare_wrapper(fitvars);
  // myid 0 divides the different datapoints over all the processors
  if (myid == 0) {
    for (i = 0; i < data_count; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send the number of the datapoint to this machine
      MPI_Ssend(&i, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }

    // tell cores that are idle to stop signaling and expecting signals
    // by sending them a stop signal -1 instead of a datapoint number
    for (i = 1; i < numprocs; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send special signal (-1) telling it to stop
      MPI_Ssend(&stopsignal, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }
  } else // any nonzero proc
  {
    for (;;) // infinite loop -until broken
    {
      // tell proc 0 that your ID is available for computations
      MPI_Ssend(&myid, 1, MPI_INT, 0, 10, MPI_COMM_WORLD);

      // receive the number of the datapoint to start working on
      MPI_Recv(&i, 1, MPI_INT, 0, 20, MPI_COMM_WORLD, &err);

      if (i >= 0) {
        calculated_flux_temp[i] = calculate_flux_wrapper(t_obs[i], nu_obs[i]);
        if (fit_host == 1) {
          int band_index = get_band_indices(nu_obs[i], host_bands, band_margin);
          if (band_index != -1) {
            if (log_host == 0) {
              calculated_flux_temp[i] += _parameters[no_fit_vars_ + band_index];
            } else {
              calculated_flux_temp[i] += pow(10, _parameters[no_fit_vars_ + band_index]);
            }
          }
        }
        chi_squaredTemp[i] = (flux_obs[i] - calculated_flux_temp[i]) * (flux_obs[i] - calculated_flux_temp[i])
            / (sigma_obs[i] * sigma_obs[i]);
      } else {
        break;
      }
    }
  }

  // We now have an array Ftemp at each processor, filled with zeroes
  // except, for each processor, the ones that particular processor has
  // calculated. We collect the sums of all individual entries at myid = 0,
  // with for each datapoint only a single processor contributing a nonzero
  // term.
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up

  MPI_Allreduce(&chi_squaredTemp[0], &chi_squared[0], data_count, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);

  double result = 0;
  for (int i = 0; i < data_count; i++) {
    result += chi_squared[i];
  }

  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
  return -1 * result;
#endif
}

double func_fitness_with_flux(std::vector<double> _parameters, void *_args, std::vector<double> &_calculated_flux) {
#if OPEN_MPI_ == ENABLED_
  boxfit_args *args = (boxfit_args *) _args;
  double fitvars[no_fit_vars_];
  for (int i = 0; i < no_fit_vars_; i++) {
    if (i == fit_theta_0_ && args->theta_0_log == 1) {
      fitvars[i] = pow(10, _parameters[i]);
    } else if (i == fit_theta_0_ && args->theta_0_log == 2) {
      fitvars[i] = exp(_parameters[i]);
    } else if (i == fit_theta_obs_ && args->theta_obs_frac) {
      fitvars[i] = _parameters[i] * fitvars[fit_theta_0_];
    } else {
      fitvars[i] = _parameters[i];
    }
  }
  if (myid == 0) {
    if (silent == 0) {
      for (int i = 0; i < no_fit_vars_; i++) {
        printf("%10e, ", fitvars[i]);
      }
      fflush(stdout);
    }
  }
  for (int i = 0; i < no_fit_vars_; i++) {
    if (fitvars[i] < hard_bounds_min[i] || fitvars[i] > hard_bounds_max[i]) {
      return -1 * std::numeric_limits<double>::max();
    }
  }
  int data_count = args->data_count;
  double *t_obs = args->t_data;
  double *nu_obs = args->nu_data;
  double *flux_obs = args->flux_data;
  double *sigma_obs = args->sigma_data;

  int i;
  int idle_id;
  MPI_Status err;
  int stopsignal = -1;

  double chi_squared[data_count];
  double chi_squaredTemp[data_count]; // temp buffer to collect results
  double calculated_flux[data_count]; // temp buffer to collect results
  double calculated_flux_temp[data_count]; // temp buffer to collect results
  _calculated_flux.resize(data_count);

  // clean out Ftemp and Fresult
  for (i = 0; i < data_count; i++) {
    chi_squared[i] = 0.0;
    chi_squaredTemp[i] = 0.0;
    calculated_flux_temp[i] = 0.0;
  }
  prepare_wrapper(fitvars);
  // myid 0 divides the different datapoints over all the processors
  if (myid == 0) {
    for (i = 0; i < data_count; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send the number of the datapoint to this machine
      MPI_Ssend(&i, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }

    // tell cores that are idle to stop signaling and expecting signals
    // by sending them a stop signal -1 instead of a datapoint number
    for (i = 1; i < numprocs; i++) {
      // wait for a proc signalling that it is idle
      MPI_Recv(&idle_id, 1, MPI_INT, MPI_ANY_SOURCE, 10, MPI_COMM_WORLD, &err);

      // send special signal (-1) telling it to stop
      MPI_Ssend(&stopsignal, 1, MPI_INT, idle_id, 20, MPI_COMM_WORLD);
    }
  } else // any nonzero proc
  {
    for (;;) // infinite loop -until broken
    {
      // tell proc 0 that your ID is available for computations
      MPI_Ssend(&myid, 1, MPI_INT, 0, 10, MPI_COMM_WORLD);

      // receive the number of the datapoint to start working on
      MPI_Recv(&i, 1, MPI_INT, 0, 20, MPI_COMM_WORLD, &err);

      if (i >= 0) {
        calculated_flux_temp[i] = calculate_flux_wrapper(t_obs[i], nu_obs[i]);
        if (fit_host == 1) {
          int band_index = get_band_indices(nu_obs[i], host_bands, band_margin);
          if (band_index != -1) {
            if (log_host == 0) {
              calculated_flux_temp[i] += _parameters[no_fit_vars_ + band_index];
            } else {
              calculated_flux_temp[i] += pow(10, _parameters[no_fit_vars_ + band_index]);
            }
          }
        }
        chi_squaredTemp[i] = (flux_obs[i] - calculated_flux_temp[i]) * (flux_obs[i] - calculated_flux_temp[i])
            / (sigma_obs[i] * sigma_obs[i]);
      } else {
        break;
      }
    }
  }

  // We now have an array Ftemp at each processor, filled with zeroes
  // except, for each processor, the ones that particular processor has
  // calculated. We collect the sums of all individual entries at myid = 0,
  // with for each datapoint only a single processor contributing a nonzero
  // term.
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
  MPI_Allreduce(&chi_squaredTemp[0], &chi_squared[0], data_count, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Allreduce(&calculated_flux_temp[0], &calculated_flux[0], data_count, MPI_DOUBLE, MPI_SUM,
                MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up

  for (int i = 0; i < data_count; i++) {
    _calculated_flux[i] = calculated_flux[i];
  }

  double result = 0;
  for (int i = 0; i < data_count; i++) {
    result += chi_squared[i];
  }
  if (myid == 0) {
    if (silent == 0) {
      printf("%10e\n", result);
      fflush(stdout);
    }
  }
  MPI_Barrier(MPI_COMM_WORLD); // wait until all cores have caught up
  return -1 * result;
#endif
}

double func_fitness_serial(std::vector<double> _parameters, void *_args) {
  boxfit_args *args = (boxfit_args *) _args;
  double fitvars[no_fit_vars_];
  for (int i = 0; i < no_fit_vars_; i++) {
    if (i == fit_theta_0_ && args->theta_0_log == 1) {
      fitvars[i] = pow(10, _parameters[i]);
    } else if (i == fit_theta_0_ && args->theta_0_log == 2) {
      fitvars[i] = exp(_parameters[i]);
    } else if (i == fit_theta_obs_ && args->theta_obs_frac) {
      fitvars[i] = _parameters[i] * fitvars[fit_theta_0_];
    } else {
      fitvars[i] = _parameters[i];
    }
  }
  for (int i = 0; i < no_fit_vars_; i++) {
    if (fitvars[i] < hard_bounds_min[i] || fitvars[i] > hard_bounds_max[i]) {
      return -1 * std::numeric_limits<double>::max();
    }
  }

  int data_count = args->data_count;
  double *t_obs = args->t_data;
  double *nu_obs = args->nu_data;
  double *flux_obs = args->flux_data;
  double *sigma_obs = args->sigma_data;

  double chi_squared[data_count];
  double calculated_flux_temp[data_count]; // temp buffer to collect results

  for (int i = 0; i < data_count; i++) {
    chi_squared[i] = 0.0;
    calculated_flux_temp[i] = 0.0;
  }

  prepare_wrapper(fitvars);
  for (int i = 0; i < data_count; i++) {
    calculated_flux_temp[i] = calculate_flux_wrapper(t_obs[i], nu_obs[i]);
    if (fit_host == 1) {
      int band_index = get_band_indices(nu_obs[i], host_bands, band_margin);
      if (band_index != -1) {
        if (log_host == 0) {
          calculated_flux_temp[i] += _parameters[no_fit_vars_ + band_index];
        } else {
          calculated_flux_temp[i] += pow(10, _parameters[no_fit_vars_ + band_index]);
        }
      }
    }
    chi_squared[i] = (flux_obs[i] - calculated_flux_temp[i]) * (flux_obs[i] - calculated_flux_temp[i])
        / (sigma_obs[i] * sigma_obs[i]);
  }

  double result = 0;
  for (int i = 0; i < data_count; i++) {
    result += chi_squared[i];
  }
  if (silent != 1) {
    printf("%1d, ", myid);
    for (int i = 0; i < no_fit_vars_; i++) {
      printf("%10e, ", fitvars[i]);
    }
    if (fit_host == 1) {
      for (int i = 0; i < host_bands.size(); i++) {
        printf("%10e, ", _parameters[no_fit_vars_ + i]);
      }
    }
    printf("%10e\n", result);
  }
  return -1 * result;
}

std::vector<int> get_band_indices(double *nu_obs, double band, int data_count, double band_margin) {
  std::vector<int> result;
  for (int i = 0; i < data_count; i++) {
    if (nu_obs[i] >= band * (1 - band_margin) && nu_obs[i] <= band * (1 + band_margin))
      result.push_back(i);
  }
  return result;
}
int get_band_indices(double nu_obs, std::vector<double> host_bands, double band_margin) {
  for (int i = 0; i < host_bands.size(); i++) {
    if (nu_obs >= host_bands[i] * (1 - band_margin) && nu_obs <= host_bands[i] * (1 + band_margin))
      return i;
  }
  return -1;
}
