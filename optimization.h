//
// Created by Mehmet Deniz Aksulu on 2019-04-02.
//

#ifndef BOXFIT_GA_OPTIMIZATION_H
#define BOXFIT_GA_OPTIMIZATION_H

#include "boxfit/src/environment.h"
#include "boxfit/src/boxfit.h"
#include "boxfit/src/param_IO.h"

#include <string>

#include <limits>
#include <vector>

typedef struct {
  int theta_0_log;

  int data_count;

  double *t_data;
  double *nu_data;
  double *flux_data;
  double *sigma_data;

  std::vector<double> min_parameters;
  std::vector<double> max_parameters;

  int theta_obs_frac;
} boxfit_args;

double func_fitness(std::vector<double> _parameters, void *_args);
double func_fitness_with_flux(std::vector<double> _parameters, void *_args, std::vector<double> &_calculated_flux);
double func_fitness_serial(std::vector<double> _parameters, void *_args);
std::vector<int> get_band_indices(double* nu_obs, double band, int data_count, double band_margin);
int get_band_indices(double nu_obs, std::vector<double> host_bands, double band_margin);

void optimize(double *var_min,
              double *var_max,
              int data_count,
              double *t_obs,
              double *nu_obs,
              double *flux_obs,
              double *sigma_obs);

#endif //BOXFIT_GA_OPTIMIZATION_H
